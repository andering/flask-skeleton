flask-skeleton
===============

This repository contains skeleton for flask modular application

- ``flask-graphql`` - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
- ``flask-jwt`` - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

flask-graphql
-------------
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

flask-jwt
---------
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

**Important**:

 |
 | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 |

Deployment
------------
Because it's advisable to run scripts  in isolated environment, here is basic guideline with use of `venv`_ (python3) and helper files ``ean.sh``, ``sitemap.sh``

.. code-block:: bash

    python3 -m venv .venv
    sh flaskskeleton.sh
..

Links
------------
- `venv`_

.. _venv: https://docs.python.org/3/library/venv.html
