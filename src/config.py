import datetime

class Default(object):
    DEBUG = False
    TESTING = False
    FLASK_ENV = None
    API_KEY = None
    SECRET_KEY = None

class Development(Default):
    DEBUG = True
    FLASK_ENV = 'development'
    API_KEY = 'cab414a5-5c05-4920-9268-84a1921ffbe4'
    SECRET_KEY = 'hgviYzAZWmD1jE4WxSp-mQ'
    JWT_SECRET_KEY = 'uMa_ztIJJuqKp3q0wgj3IFKgpPhYf4gSaCI3nLhNXl9rIL4at8v4n8NV2yN0nZPxhlqjTvMa7FYfibIrvzQv42lrV439ThZKAYxNcWAd5Cg-Xu-b8IDUhpBXFbjhsA7_WALtRTVWfq2wDcx_dMfkmJyFh9vKjFpOiY99KYDNAccOOg7VuLxxqH_rmxVk9E28UnmxyOxNtCtfSgiKYSrgoVS-_buJJHuoJuJzGKZRmfF6lI4ajIkbtXVx-MG3Hg31fSoEcHvKP0Bb4uGkV8igr29IyDtjdNCK_pVhJuAZllHShuVx1mHvRcIFdPGXhvf18sk7I3dIH7YeIaPQLUy6sw'
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(days=1)

class Production(Default):
    FLASK_ENV = 'production'
    API_KEY = 'cab414a5-5c05-4920-9268-84a1921ffbe4'
    SECRET_KEY = 'hgviYzAZWmD1jE4WxSp-mQ'
    JWT_SECRET_KEY = 'uMa_ztIJJuqKp3q0wgj3IFKgpPhYf4gSaCI3nLhNXl9rIL4at8v4n8NV2yN0nZPxhlqjTvMa7FYfibIrvzQv42lrV439ThZKAYxNcWAd5Cg-Xu-b8IDUhpBXFbjhsA7_WALtRTVWfq2wDcx_dMfkmJyFh9vKjFpOiY99KYDNAccOOg7VuLxxqH_rmxVk9E28UnmxyOxNtCtfSgiKYSrgoVS-_buJJHuoJuJzGKZRmfF6lI4ajIkbtXVx-MG3Hg31fSoEcHvKP0Bb4uGkV8igr29IyDtjdNCK_pVhJuAZllHShuVx1mHvRcIFdPGXhvf18sk7I3dIH7YeIaPQLUy6sw'
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(minutes=15)

class Testing(Default):
    TESTING = True


config = {
    'development': Development,
    'testing': Testing,
    'production': Production
}