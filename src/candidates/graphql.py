import graphene


class Query(graphene.ObjectType):
    candidate = graphene.String()

    def resolve_candidate(parent, info):
        return 'candidate'
