from .. entity import Entity



import pymongo


class Validator(object):
    @classmethod
    def required(cls,items):
        for item in items:
            if item is None:
                raise Exception('REQUIRED')
    @classmethod
    def length(cls,min_ = None, max_ = None, items):
        for item in items:
            if(min_ is not None):
                if len(item) < min_:
                    raise Exception('LENGTH_MIN')
            if(max_ is not None):
                if len(item) > max_:
                    raise Exception('LENGTH_MAX')



class Entity(object):

    _uri = "mongodb://127.0.0.1:27017"

    def __init__(self):
        client = pymongo.MongoClient(self._uri)
        self._db = client['test']

    def persist(self):
        self.validate()
        self._db[self.collection].insert(self.json())


class Candidate(Entity):
    _collection = 'candidates'

    first_name = None
    last_name = None
    realname = None

    '''
    wrong_cell_phone = None
    source = None
    is_abroad = None
    role = None
    desired_pay = None
    current_pay = None
    best_time_to_call = None
    gdpr_sourcing = None
    gdpr_cv = None
    gdpr_delete = None
    notes = None
    info = None
    created = None
    modified = None
    owner = None
    entered_by = None
    address = None
    phones = None
    emails = None
    '''

    employer = None
    key_skills = None

    def __init__(self):
        super().__init__()

    def validate(self):
        Validator.required([self.first_name, self.last_name, self.employer, self.key_skills])
        Validator.lenght(2,50,[self.first_name, self.last_name, self.realname])

    def json(self):
        return {k:v for k, v in self.__dict__.items() if k[0] != '_'}
