import pymongo


class Entity(object):

    URI = "mongodb://127.0.0.1:27017"

    def __init__(self):
        client = pymongo.MongoClient(self.URI)
        self.db = client['test']

    def persist(self):
        self.validate()
        self.db[self.collection].insert(self.json())

