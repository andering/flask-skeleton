# -*- coding: UTF-8 -*-
"""
Auth.graphql.

Graphql | Controller | Entering-point for auth module

:copyright: andering@gmail.com
:license: BSD-3-Clause
"""

import graphene
import tempfile
import textract
from base64 import b64decode


class Type(graphene.Enum):
    """
    Type.

    Type to use with graphene
    """
class Query(graphene.ObjectType):
    """
    Query.

    Query to use with graphene
    """

    bytes = graphene.Field(
        type=graphene.String,
        bytes=graphene.Argument(graphene.String),
        extension=graphene.Argument(Type)
    )

    def resolve_bytes(parent, info, bytes, extension):
        """
        Bytes handler.

        Handling bytes entering point
        """
        temp = tempfile.NamedTemporaryFile(suffix='.pdf')
        try:
            temp.write(b64decode(bytes))
            text = textract.process(temp.name).decode('utf-8')
        finally:
            temp.close()

        return text
